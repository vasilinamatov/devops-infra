# Devops Infra

### Prepare your working Environment

- Spin up your Cloud Instance or Virtual Box with Ubuntu 20.04
- Install git and run git clone for this repository
- Run the provisioning.sh file to install all required packages on your VM
- It will update the Ubuntu, install some Tools and some packages of docker, aws cli, aws-authenticator, eksctl, kubectl, helm...
- Use aws configure to configure your aws credentials
- Create EC2 Key Pair aws ec2 create-key-pair --key-name <NameYourKey> for further use
- Deploy gitlab-runner on docker inside your VM to be used by GitLab
- On Push the CI/CD Pipeline will run and deploy EKS Cluster via Terraform Code

### Output of the dockers running on VM

```
root@osboxes:~# docker ps
CONTAINER ID   IMAGE                         COMMAND                  CREATED          STATUS          PORTS           NAMES
b945a9b9596a   compose_flask_web             "/bin/sh -c 'python …"   4 minutes ago    Up 4 seconds    0.0.0.0:80->80/tcp, :::80->80/tcp   compose_flask_web_1
02906098ee34   81f5749c9058                  "docker-entrypoint.s…"   7 minutes ago    Up 7 minutes                    runner-kuzywrzt-project-28876019-concurrent-0-72a1848b075b81e5-build-2
de87387c0c82   66dc2d45749a                  "dockerd-entrypoint.…"   8 minutes ago    Up 8 minutes    2375-2376/tcp   runner-kuzywrzt-project-28876019-concurrent-0-72a1848b075b81e5-docker-0
bc18f94d5194   gitlab/gitlab-runner:latest   "/usr/bin/dumb-init …"   19 minutes ago   Up 10 minutes                   gitlab-runner
```

<br>
Good Luck!